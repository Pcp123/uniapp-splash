自定义开屏启动广告,点击进入广告页,自动关闭,手动关闭.

使用说明：参考自大佬 https://ext.dcloud.net.cn/plugin?id=206,进行添加了点击跳转广告事件

下载后解压，直接可在HbuilderX中运行。只能在app端运行.

原理：使用5+api：plus.webview.open()创建一个webview,然后加载一个本地网页，网页可做成广告页的样式。网页中引入uni的SDK,使网页具有5+api的能力，可在点击网页中的按钮关闭创建的webview。

Q：如何运用在自己的项目？

A：将App.vue页面中onLaunch周期函数中的创建及关闭webview的代码拷贝至自己项目的相应位置；将根目录下的hybird目录拷贝至自己项目的根目录下。